const redux = require('redux')
const createStore = redux.createStore

const initialState = {
    myListData: [
        {
            myVal: 0
        },
    ],
    myAnotherListData: [
        {
            myVal: 0
        },
    ],
}

// constants
const INC_COUNT = 'INC_COUNT'

// reducers - simple javascript functions but it's a pure functions
const rootReducer = (state = initialState, action) => {
    if(action.type === INC_COUNT) {
        return {
            ...state,
            myListData: [...state.myListData, { myVal: action.value }]
        }
    }
    return state
}

// store
const store = createStore(rootReducer)
console.log(store.getState())

// action-creators
const incCountFn = (value) => {
    return {
        type: INC_COUNT,
        value
    }
}

// actions
/* const incrementCount1 = {
    type: INC_COUNT,
    value: 1
}
const incrementCount2 = {
    type: INC_COUNT,
    value: 2
}
const incrementCount3 = {
    type: INC_COUNT,
    value: 3
} */

// dispatch
store.dispatch(incCountFn(1))
store.dispatch(incCountFn(2))
store.dispatch(incCountFn(3))

console.log(store.getState())