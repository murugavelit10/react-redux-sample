import React, { Component } from 'react';
import { connect } from 'react-redux';
import Constants from './constants/index';

class App extends Component {
  render() {
    return (
      <div>
        Count: {this.props.cnt}
        <br />
        <button onClick={this.props.incOne}>Increment Count</button>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cnt: state.count
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    incOne: () => dispatch({type: Constants.INC_COUNT, value: 1})
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
