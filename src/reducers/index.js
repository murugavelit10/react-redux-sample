import Constants from '../constants/index';

const initialState = {
    todos: [],
}

export default (state = initialState, action) => {
    switch(action.type) {
        case Constants.ADD_TODO:
            return {
                todos: [...state.todos, action.payload]
            }
        default:
            return state
    }
}