import React, { Component } from 'react';
import { connect } from 'react-redux';
import TodoItem from './TodoItem';

class TodoList extends Component {
    render() {
        console.log(this.props.todos)
        return (
            <ul>
                {
                    this.props.todos.map((todo) => <TodoItem {...todo} />)
                }
                
            </ul>
        );
    }
}

const mapStateToProps = state => {
    return {
        todos: state.todos
    }
}

export default connect(mapStateToProps)(TodoList);