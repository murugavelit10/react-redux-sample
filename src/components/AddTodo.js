import React, { Component } from 'react';
import { connect } from 'react-redux';
import Constants from '../constants/index';

class AddTodo extends Component {
    state = {
        todoInputValue: ''
    }

    onAddTodoBtnClick = () => {
        const todo = this.state.todoInputValue
        this.setState({todoInputValue: ''})
        this.props.addTodo(todo)
    }

    updateTodoInputValue = (e) => {
        this.setState({todoInputValue: e.target.value})
    }


    render() {
        return (
            <div>
                <input onChange={this.updateTodoInputValue} value={this.state.todoInputValue} />
                <input type="button" value="Add Todo" onClick={this.onAddTodoBtnClick} />
            </div>
        );
    }
}

const mapDispatchToProps = dispatch => {
    return {
        addTodo: (todo) => {
            return dispatch({type: Constants.ADD_TODO, payload: { todo, isCompleted: false }})
        }
    }
}

export default connect(null, mapDispatchToProps)(AddTodo);