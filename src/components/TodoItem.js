import React, { Component } from 'react';

class TodoItem extends Component {
    render() {
        return (
            <li>
                {this.props.todo} - {this.props.isCompleted ? 'TRUE' : 'FALSE'}
            </li>
        );
    }
}

export default TodoItem;