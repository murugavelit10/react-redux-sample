import React, { Component } from 'react';
import AddTodo from '../components/AddTodo';
import TodoList from './TodoList';

class Todos extends Component {
    render() {
        return (
            <div>
                <h1>Todos</h1>
                <AddTodo />
                <TodoList />
            </div>
        );
    }
}

export default Todos;